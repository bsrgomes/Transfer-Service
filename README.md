## Transfer Service

## Used Technologies

I decided to use Spring Boot because it’s easy to create applications that you can just run with less configuration as possible.

The input done via REST api that receives and returns JSON objects and the database is a H2 in memory database.


## API
	
	- Create Account
	URL
		/ingenico/api/account/create
	Method:
		POST
	URL Params
		Required:
			name=[String],
			balance=[BigDecimal]
 	Success Response:
			Code: 200
    		Content: {"id":46,"name":"Acc Name","balance":100,"lastUpdate":1514055726613}
		OR	
			Code: 400
    		Content: {“Account already exists!”}

    - Transfer Funds
    URL
		/ingenico/api/account/transfer
	Method:
		POST
	URL Params
		Required:
			accountFrom=[String],
			accountTo=[String],
			amount=[BigDecimal]
 	Success Response:
    		Code: 200
    		Content: {"id":72,"nameFrom":"Acc From","nameTo":"Acc To","amount":1,"transferTime":1514057056154}
		OR	
			Code: 400
    		Content: {“Account not found!”}
    	OR	
			Code: 400
    		Content: {“Insufficient funds to transfer!”}
    	OR	
			Code: 400
    		Content: {“The destination and source account cannot be the same!”}

## How to Run?

The maven command to run the application:

	mvn spring-boot:run

The application will be running at 

	http://localhost:8080/ingenico

## JMeter Tests

The applications was able to deal with more than 100 transfer requests per second and the response time average was 1100ms.

## How to scale?

If it is necessary to escalate the application in order to deal with more requests per minute, we can easily deploy it to a Docker Container and start additional instances.
It will require architectural changes like a centralised database and a Service registration and discovery like Eureka and a proxy like Zuul to handle the multiple instances of the application.