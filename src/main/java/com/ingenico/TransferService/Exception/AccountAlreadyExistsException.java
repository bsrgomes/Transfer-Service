package com.ingenico.TransferService.Exception;

public class AccountAlreadyExistsException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = -6627054956230076401L;

  public AccountAlreadyExistsException() {
    super("Account already exists!");
  }
}
