package com.ingenico.TransferService.Exception;

public class AccountNotFoundException extends Exception {


  /**
   * 
   */
  private static final long serialVersionUID = -483249885811410515L;

  public AccountNotFoundException() {
    super("Account not found!");
  }
}
