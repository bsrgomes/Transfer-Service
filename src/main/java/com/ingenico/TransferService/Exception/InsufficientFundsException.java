package com.ingenico.TransferService.Exception;

public class InsufficientFundsException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = -6627054956230076401L;

  public InsufficientFundsException() {
    super("Insufficient funds!");
  }
}
