package com.ingenico.TransferService.Exception;

public class SameAccountTransferException extends Exception {


  /**
   * 
   */
  private static final long serialVersionUID = 309292250986207469L;

  public SameAccountTransferException() {
    super("The destination and source account cannot be the same!");
  }
}
