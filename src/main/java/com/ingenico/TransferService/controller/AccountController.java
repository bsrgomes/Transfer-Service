package com.ingenico.TransferService.controller;

import java.math.BigDecimal;

import net.sf.aspect4log.Log;
import net.sf.aspect4log.Log.Exceptions;
import net.sf.aspect4log.Log.Level;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenico.TransferService.Exception.AccountAlreadyExistsException;
import com.ingenico.TransferService.Exception.AccountNotFoundException;
import com.ingenico.TransferService.Exception.InsufficientFundsException;
import com.ingenico.TransferService.Exception.SameAccountTransferException;
import com.ingenico.TransferService.domain.Account;
import com.ingenico.TransferService.domain.TransferAudit;
import com.ingenico.TransferService.service.AccountService;


@RestController
@Log(enterLevel = Level.DEBUG, exitLevel = Level.DEBUG, on = @Exceptions(level = Level.DEBUG))
@RequestMapping("/api/account")
public class AccountController {

  @Autowired
  private AccountService accountService;

  /**
   * Create new account
   * 
   * @param name: Account name
   * @param balance: Account balance
   * @return
   * @throws AccountAlreadyExistsException if the account already exists
   */
  @ResponseBody
  @RequestMapping(path = "/create", method = RequestMethod.POST, produces = "application/json")
  public ResponseEntity<Account> createAccount(
      @RequestParam(name = "name", required = true) String name, @RequestParam(name = "balance",
          required = true) BigDecimal balance) throws AccountAlreadyExistsException {

    Account account = accountService.createAccount(name, balance);

    return ResponseEntity.ok(account);
  }

  /**
   * Transfer funds between 2 accounts
   * 
   * @param accountFrom: Origin account
   * @param accountTo: Destination account
   * @param amount: amount money to be transfered
   * @return TransferAudit
   * @throws AccountNotFoundException if the account cannot be found
   * @throws InsufficientFundsException if the account have insufficient funds
   * @throws SameAccountTransferException if the origin account is the same as destination
   */
  @ResponseBody
  @RequestMapping(path = "/transfer", method = RequestMethod.POST, produces = "application/json")
  public ResponseEntity<TransferAudit> transfer(
      @RequestParam(name = "accountFrom", required = true) String accountFrom, @RequestParam(
          name = "accountTo", required = true) String accountTo, @RequestParam(name = "amount",
          required = true) BigDecimal amount) throws AccountNotFoundException,
      InsufficientFundsException, SameAccountTransferException {

    TransferAudit audit = accountService.transfer(accountFrom, accountTo, amount);

    return ResponseEntity.ok(audit);
  }

}
