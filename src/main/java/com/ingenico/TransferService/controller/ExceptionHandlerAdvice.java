package com.ingenico.TransferService.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.ingenico.TransferService.Exception.AccountAlreadyExistsException;
import com.ingenico.TransferService.Exception.AccountNotFoundException;
import com.ingenico.TransferService.Exception.InsufficientFundsException;
import com.ingenico.TransferService.Exception.SameAccountTransferException;

@ControllerAdvice
public class ExceptionHandlerAdvice {

  @ExceptionHandler(AccountAlreadyExistsException.class)
  public ResponseEntity handleException(AccountAlreadyExistsException e) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
  }

  @ExceptionHandler(AccountNotFoundException.class)
  public ResponseEntity handleException(AccountNotFoundException e) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
  }

  @ExceptionHandler(InsufficientFundsException.class)
  public ResponseEntity handleException(InsufficientFundsException e) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
  }

  @ExceptionHandler(SameAccountTransferException.class)
  public ResponseEntity handleException(SameAccountTransferException e) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
  }
}
