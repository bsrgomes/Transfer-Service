package com.ingenico.TransferService.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "transfer_audit")
public class TransferAudit {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "name_from", nullable = false)
  private String nameFrom;

  @Column(name = "name_to", nullable = false)
  private String nameTo;

  @Column(name = "amount", nullable = false)
  private BigDecimal amount;

  @Column(name = "transfer_time", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date transferTime;

  public TransferAudit() {}

  public TransferAudit(String nameFrom, String nameTo, BigDecimal amount, Date transferTime) {
    this.nameFrom = nameFrom;
    this.nameTo = nameTo;
    this.amount = amount;
    this.transferTime = transferTime;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNameFrom() {
    return nameFrom;
  }

  public void setNameFrom(String nameFrom) {
    this.nameFrom = nameFrom;
  }

  public String getNameTo() {
    return nameTo;
  }

  public void setNameTo(String nameTo) {
    this.nameTo = nameTo;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Date getTransferTime() {
    return transferTime;
  }

  public void setTransferTime(Date transferTime) {
    this.transferTime = transferTime;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((amount == null) ? 0 : amount.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((nameFrom == null) ? 0 : nameFrom.hashCode());
    result = prime * result + ((nameTo == null) ? 0 : nameTo.hashCode());
    result = prime * result + ((transferTime == null) ? 0 : transferTime.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    TransferAudit other = (TransferAudit) obj;
    if (amount == null) {
      if (other.amount != null)
        return false;
    } else if (!amount.equals(other.amount))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (nameFrom == null) {
      if (other.nameFrom != null)
        return false;
    } else if (!nameFrom.equals(other.nameFrom))
      return false;
    if (nameTo == null) {
      if (other.nameTo != null)
        return false;
    } else if (!nameTo.equals(other.nameTo))
      return false;
    if (transferTime == null) {
      if (other.transferTime != null)
        return false;
    } else if (!transferTime.equals(other.transferTime))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "TransferAudit [id=" + id + ", nameFrom=" + nameFrom + ", nameTo=" + nameTo
        + ", amount=" + amount + ", transferTime=" + transferTime + "]";
  }

}
