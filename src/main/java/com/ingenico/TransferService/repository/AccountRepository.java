package com.ingenico.TransferService.repository;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ingenico.TransferService.domain.Account;

public interface AccountRepository extends CrudRepository<Account, Long> {

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  @Query("SELECT a FROM com.ingenico.TransferService.domain.Account a WHERE a.name = :#{#name}")
  Account findOneForUpdate(@Param("name") String name);
  
  @Query("SELECT a FROM com.ingenico.TransferService.domain.Account a WHERE a.name = :#{#name}")
  Account findByName(@Param("name") String name);

}
