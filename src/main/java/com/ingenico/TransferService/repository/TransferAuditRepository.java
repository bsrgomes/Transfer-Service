package com.ingenico.TransferService.repository;

import org.springframework.data.repository.CrudRepository;

import com.ingenico.TransferService.domain.TransferAudit;

public interface TransferAuditRepository extends CrudRepository<TransferAudit, Long> {
}