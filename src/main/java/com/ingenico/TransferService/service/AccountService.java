package com.ingenico.TransferService.service;

import java.math.BigDecimal;

import com.ingenico.TransferService.Exception.AccountAlreadyExistsException;
import com.ingenico.TransferService.Exception.AccountNotFoundException;
import com.ingenico.TransferService.Exception.InsufficientFundsException;
import com.ingenico.TransferService.Exception.SameAccountTransferException;
import com.ingenico.TransferService.domain.Account;
import com.ingenico.TransferService.domain.TransferAudit;

public interface AccountService {

  /**
   * Create new account
   * 
   * @param name: name of the user
   * @param balance: balance of the account
   * @return
   * @throws AccountAlreadyExistsException throws exception if account already exists
   */
  Account createAccount(String name, BigDecimal balance) throws AccountAlreadyExistsException;

  /**
   * Return account
   * 
   * @param name: Account name
   * @return
   * @throws AccountNotFoundException if not found
   */
  Account getAccount(String name) throws AccountNotFoundException;

  /**
   * Transfer amount between accounts
   * 
   * @param accountFrom: account to deduce balance
   * @param accountTo: account to credit balance
   * @param amount: transfer amount
   * @return TransferAudit
   * @throws AccountNotFoundException
   * @throws InsufficientFundsException
   * @throws SameAccountTransferException
   */
  TransferAudit transfer(String accountFrom, String accountTo, BigDecimal amount)
      throws AccountNotFoundException, InsufficientFundsException, SameAccountTransferException;
}
