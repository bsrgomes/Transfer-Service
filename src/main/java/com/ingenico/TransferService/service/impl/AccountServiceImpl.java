package com.ingenico.TransferService.service.impl;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import net.sf.aspect4log.Log;
import net.sf.aspect4log.Log.Exceptions;
import net.sf.aspect4log.Log.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.ingenico.TransferService.Exception.AccountAlreadyExistsException;
import com.ingenico.TransferService.Exception.AccountNotFoundException;
import com.ingenico.TransferService.Exception.InsufficientFundsException;
import com.ingenico.TransferService.Exception.SameAccountTransferException;
import com.ingenico.TransferService.domain.Account;
import com.ingenico.TransferService.domain.TransferAudit;
import com.ingenico.TransferService.repository.AccountRepository;
import com.ingenico.TransferService.repository.TransferAuditRepository;
import com.ingenico.TransferService.service.AccountService;

@Service
@Scope("prototype")
public class AccountServiceImpl implements AccountService {

  @Autowired
  private AccountRepository accountRepository;

  @Autowired
  private TransferAuditRepository transferAuditRepository;

  private final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

  @Override
  @Log(enterLevel = Level.INFO, on = @Exceptions(level = Level.INFO), indent = true)
  public Account createAccount(String name, BigDecimal balance)
      throws AccountAlreadyExistsException {

    Account acc = new Account(name, balance);

    try {
      acc = accountRepository.save(acc);
    } catch (DataIntegrityViolationException e) {
      throw new AccountAlreadyExistsException();
    }

    return acc;
  }


  @Override
  @Transactional
  @Log(enterLevel = Level.INFO, on = @Exceptions(level = Level.INFO), indent = true)
  public TransferAudit transfer(String accountFrom, String accountTo, BigDecimal amount)
      throws AccountNotFoundException, InsufficientFundsException, SameAccountTransferException {
    // check if accounts are different
    if (accountFrom.equals(accountTo)) {
      throw new SameAccountTransferException();
    }

    // get accounts
    Account accFrom = this.getAccountForUpdate(accountFrom);
    Account accTo = this.getAccountForUpdate(accountTo);

    // check if the account has enough funds to transfer
    checkFunds(accFrom, amount);

    // transfer funds
    transferFunds(accFrom, accTo, amount);

    // save accounts
    accFrom = accountRepository.save(accFrom);
    accTo = accountRepository.save(accTo);

    // save audit
    return saveTransferAudit(accFrom, accTo, amount);
  }

  /**
   * Transfer amount between accounts
   * 
   * @param accFrom: Origin account
   * @param accTo: Destination account
   * @param amount: Amount to be transfered
   */
  private void transferFunds(Account accFrom, Account accTo, BigDecimal amount) {
    accFrom.setBalance(accFrom.getBalance().subtract(amount));
    accTo.setBalance(accTo.getBalance().add(amount));
  }

  /**
   * Check if the account has funds
   * 
   * @param accFrom
   * @param amount
   * @throws InsufficientFundsException if has insufficient funds
   */
  private void checkFunds(Account accFrom, BigDecimal amount) throws InsufficientFundsException {
    if (accFrom.getBalance().compareTo(amount) < 0) {
      throw new InsufficientFundsException();
    }
  }

  /**
   * Get account
   * 
   * @param accountName
   * @return
   * @throws AccountNotFoundException if account cannot be found
   */
  public Account getAccount(String accountName) throws AccountNotFoundException {
    Account acc = accountRepository.findByName(accountName);

    if (acc == null) {
      throw new AccountNotFoundException();
    }

    return acc;
  }
  
  /**
   * Get account for update
   * 
   * @param accountName
   * @return
   * @throws AccountNotFoundException if account cannot be found
   */
  private Account getAccountForUpdate(String accountName) throws AccountNotFoundException {
    Account acc = accountRepository.findOneForUpdate(accountName);
    
    if (acc == null) {
      throw new AccountNotFoundException();
    }
    
    return acc;
  }


  /**
   * Register transaction audit
   * 
   * @param accountFrom
   * @param accountTo
   * @param amount
   */
  private TransferAudit saveTransferAudit(Account accountFrom, Account accountTo, BigDecimal amount) {
    TransferAudit audit =
        new TransferAudit(accountFrom.getName(), accountTo.getName(), amount,
            accountFrom.getLastUpdate());

    logger.info(audit.toString());
    return transferAuditRepository.save(audit);
  }
}
