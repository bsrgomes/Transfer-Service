package com.ingenico.transferservice.TransferService.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ingenico.TransferService.TransferServiceApplication;
import com.ingenico.TransferService.domain.Account;
import com.ingenico.TransferService.service.AccountService;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT,
    classes = TransferServiceApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.yml")
public class AccountControllerTest {

  @Autowired
  private MockMvc mvc;
  
  @Autowired
  private AccountService accountService;

  @Test
  public void accountAlreadyExistsTest() throws Exception {
    mvc.perform(post("/api/account/create")
        .param("name", "account1")
        .param("balance","10000000.0")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content()
        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    
    mvc.perform(post("/api/account/create")
        .param("name", "account1")
        .param("balance","10000000.0")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(content()
        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", is("Account already exists!")));
  }
  
  @Test
  public void transferWithoutFundsTest() throws Exception {
    mvc.perform(post("/api/account/create")
        .param("name", "account99")
        .param("balance","0.0")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content()
        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    
    mvc.perform(post("/api/account/create")
        .param("name", "account98")
        .param("balance","0.0")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content()
        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    
    mvc.perform(post("/api/account/transfer")
        .param("accountFrom", "account99")
        .param("accountTo", "account98")
        .param("amount","1.0")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(content()
        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", is("Insufficient funds!")));
    
  }
  
  @Test
  public void transferSameAccountTest() throws Exception {
    mvc.perform(post("/api/account/create")
        .param("name", "account50")
        .param("balance","0.0")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content()
        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    
    mvc.perform(post("/api/account/transfer")
        .param("accountFrom", "account50")
        .param("accountTo", "account50")
        .param("amount","1.0")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(content()
        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", is("The destination and source account cannot be the same!")));
    
  }
  
  @Test
  public void transferTest() throws Exception {
    mvc.perform(post("/api/account/create")
        .param("name", "account2")
        .param("balance","10000000.0")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content()
        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    
    mvc.perform(post("/api/account/create")
        .param("name", "account3")
        .param("balance","0.0")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content()
        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    
    for (int i = 0; i < 100; i++) {
      mvc.perform(post("/api/account/transfer")
          .param("accountFrom", "account2")
          .param("accountTo", "account3")
          .param("amount","1.0")
          .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(content()
          .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
    Account acc = accountService.getAccount("account3");
    Assert.assertTrue(acc.getBalance().compareTo(new BigDecimal(100.0)) == 0);
  }
}
